package com.example.stefanstrasser.motogp2015;

import android.graphics.Bitmap;

import java.util.Date;

/**
 * Created by Stefan Strasser on 28.02.2015.
 */
public class MotoGPDriver {
    private int Id;
    private int StartNumber;
    private String FirstName;
    private String LastName;
    private String Country;
    private String Team;
    private String Motorcycle;
    private Date Birthday;
    private int Weight;
    private int Height;
    private String CountryImageUrl;
    private Bitmap CountryImage;
    private String PictureUrl;
    private Bitmap Picture;

    public int getStartNumber() {
        return StartNumber;
    }

    public void setStartNumber(int startNumber) {
        StartNumber = startNumber;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getTeam() {
        return Team;
    }

    public void setTeam(String team) {
        Team = team;
    }

    public String getMotorcycle() {
        return Motorcycle;
    }

    public void setMotorcycle(String motorcycle) {
        Motorcycle = motorcycle;
    }

    public Date getBirthday() {
        return Birthday;
    }

    public void setBirthday(Date birthday) {
        Birthday = birthday;
    }

    public int getWeight() {
        return Weight;
    }

    public void setWeight(int weight) {
        Weight = weight;
    }

    public int getHeight() {
        return Height;
    }

    public void setHeight(int height) {
        Height = height;
    }

    public String getCountryImageUrl() {
        return CountryImageUrl;
    }

    public void setCountryImageUrl(String countryImageUrl) {
        CountryImageUrl = countryImageUrl;
    }

    public Bitmap getCountryImage() {
        return CountryImage;
    }

    public void setCountryImage(Bitmap countryImage) {
        CountryImage = countryImage;
    }

    public String getPictureUrl() {
        return PictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        PictureUrl = pictureUrl;
    }

    public Bitmap getPicture() {
        return Picture;
    }

    public void setPicture(Bitmap picture) {
        Picture = picture;
    }
}
