package com.example.stefanstrasser.motogp2015;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends FragmentActivity implements ActionBar.TabListener {
    private ProgressDialog progressDialog;
    private CustomListAdapter adapter;
    private ListView listView;
    private List<MotoGPDriver> drivers = new ArrayList<MotoGPDriver>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.list);
        adapter = new CustomListAdapter(this, drivers);
        listView.setAdapter(adapter);
        Load();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Toast.makeText(getApplicationContext(), "Test", Toast.LENGTH_LONG).show();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == R.id.action_refresh) {
            Load();
        }

        if(id == R.id.action_about) {
            AboutDialog();
        }

        return super.onOptionsItemSelected(item);
    }

    public void Load() {
        drivers.clear();
        adapter.notifyDataSetChanged();
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Lädt...");
        progressDialog.show();
        String url = "http://www.stefanstrasser.com/motogp/getAllMotoGPDrivers.php";
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        hideProgressDialog();
                        for(int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);
                                if(obj.length() == 0) {
                                    Toast.makeText(getApplicationContext(), "NULL", Toast.LENGTH_LONG).show();
                                }
                                final MotoGPDriver driver = new MotoGPDriver();
                                driver.setStartNumber(obj.getInt("StartNumber"));
                                driver.setFirstName(obj.getString("FirstName"));
                                driver.setLastName(obj.getString("LastName"));
                                driver.setCountry(obj.getString("Country"));
                                driver.setTeam(obj.getString("Team"));
                                driver.setMotorcycle(obj.getString("Motorcycle"));
                                driver.setCountryImageUrl("http://www.stefanstrasser.com/motogp/flags/" + driver.getCountry() + "-Flag-24.png");
                                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                                try {
                                    driver.setBirthday(formatter.parse(obj.getString("Birthday")));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                driver.setHeight(obj.getInt("Height"));
                                driver.setWeight(obj.getInt("Weight"));
                                drivers.add(driver);
                            } catch(JSONException ex) {
                                ex.printStackTrace();
                            }
                        }
                        adapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonArrayRequest);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hideProgressDialog();
    }

    private void hideProgressDialog() {
        if(progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    private void AboutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.about_dialog, null));
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }
}
