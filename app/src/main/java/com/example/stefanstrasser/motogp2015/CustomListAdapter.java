package com.example.stefanstrasser.motogp2015;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by Stefan Strasser on 28.02.2015.
 */
public class CustomListAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<MotoGPDriver> drivers;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public CustomListAdapter(Activity activity, List<MotoGPDriver> drivers) {
        this.activity = activity;
        this.drivers = drivers;
    }

    @Override
    public int getCount() {
        return drivers.size();
    }

    @Override
    public Object getItem(int location) {
        return drivers.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(inflater == null) {
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if(convertView == null) {
            convertView = inflater.inflate(R.layout.list_row, null);
        }
        if(imageLoader == null) {
            imageLoader = AppController.getInstance().getImageLoader();

        }
        NetworkImageView thumbnail = (NetworkImageView) convertView.findViewById(R.id.thumbnail);
        NetworkImageView countryImage = (NetworkImageView) convertView.findViewById(R.id.country);
        TextView startnumber = (TextView) convertView.findViewById(R.id.startnumber);
        TextView team = (TextView) convertView.findViewById(R.id.team);
        TextView firstname = (TextView) convertView.findViewById(R.id.firstname);
        TextView lastname = (TextView) convertView.findViewById(R.id.lastname);
        TextView birthday = (TextView) convertView.findViewById(R.id.birthday);
        TextView body = (TextView) convertView.findViewById(R.id.body);

        MotoGPDriver driver = drivers.get(position);
        thumbnail.setImageUrl("http://www.stefanstrasser.com/motogp/MotoGP_Drivers/"+driver.getStartNumber()+".jpg", imageLoader);
        countryImage.setImageUrl(driver.getCountryImageUrl(), imageLoader);
        if(driver.getStartNumber() < 10) {
            startnumber.setText("#0" + driver.getStartNumber() + " | ");
        }
        else {
            startnumber.setText("#" + driver.getStartNumber() + " | ");
        }
        firstname.setText(driver.getFirstName());
        lastname.setText(driver.getLastName());
        team.setText(driver.getTeam() + ", " + driver.getMotorcycle());
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        birthday.setText(String.valueOf(formatter.format(driver.getBirthday())) + " - ");
        body.setText(driver.getHeight() + " cm, " + driver.getWeight() + " kg");
        return convertView;
    }
}
